import flask
from flask import request
import arrow 
import acp_times
import config

import logging
    #----------------------------------------------------------------------------------------------------------


app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY
###
# Pages
###                                 
                                                                                              
                                                                                                                           


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')

#----------------------------------------------------------------------------------------------------------
@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


    #----------------------------------------------------------------------------------------------------------
@app.route("/_calc_times")
def _calc_times():





    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    
    
    begin_date = request.args.get('begin_date')
    console.log("begin date: " + begin_date)
    begin_time = request.args.get('begin_time')
    console.log("begin_time: " + begin_date)

    begin = begin_date + " " + begin_time
    console.log("begin: " + begin)
    time = arrow.get(begin, 'YYYY-MM-DD HH:mm')

    brevet = request.args.get('distance')
    open_time = acp_times.open_time(km, brevet, time.isoformat())
    #send datas

    close_time = acp_times.close_time(km, brevet, time.isoformat())
    #send dates 
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)
#----------------------------------------------------------------------------------------------------------








app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
