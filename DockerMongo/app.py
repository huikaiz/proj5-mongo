import os
from flask import Flask, redirect, url_for, request, render_template
from pymongo import MongoClient
import arrow
import flask
import logging
import acp_time
import config



    #----------------------------------------------------------------------------------------------------------
app = Flask(__name__)
client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY
@app.route("/")
@app.route("/index")




    #----------------------------------------------------------------------------------------------------------
def index():
    return render_template('calc.html')
@app.route('/reset', methods =['POST'])






    #----------------------------------------------------------------------------------------------------------
def reset():
    db.tododb.remove({})#remove all entery
    return render_template('calc.html')
@app.route('/display', methods =['POST'])





    #----------------------------------------------------------------------------------------------------------
def todo():
    _items = db.tododb.find()
    items = [item for item in _items]
    return render_template('todo.html', items=items)
    ##gathers all entry
    #send html
@app.route('/submit', methods=['POST'])





    #----------------------------------------------------------------------------------------------------------
def submit():




    _open = request.form.getlist('open')
    _close = request.form.getlist('close')
    open_data = []
    close_data = []
    #add form to database
#gather data to open time
    for thing in _open:
        if str(thing) != '':
            open_data.append(str(thing))
            app.logger.debug("Added: " + str(thing))
            #list add close time 



    #----------------------------------------------------------------------------------------------------------
    for thing in _close:
        if str(thing) != '':
            close_data.append(str(thing))
            app.logger.debug("Added: " + str(thing))
            #add each close time to a list




            #----------------------------------------------------------------------------------------------------------
    for i in range(len(open_data)):
        things = {'open_times': open_data[i], 'close_times': close_data[i]}
        db.tododb.insert_one(things)
        app.logger.debug("Inserted: " + str(things))
    _entries = db.tododb.find()
    entries = [entry for entry in _entries]# check database is nothing 
    if entries != []:
        return redirect(url_for('index'))
    else:
        return render_template('nothing.html')





            #----------------------------------------------------------------------------------------------------------





@app.errorhandler(404)

def page_not_found(error):
    flask.session['linkback'] = flask.url_for('index')
    return flask.render_template('404.html')






        #----------------------------------------------------------------------------------------------------------
@app.route("/nothing")
def nothing():
    return render_template('nothing.html')






        #----------------------------------------------------------------------------------------------------------
@app.route("/_calc_times")
def _calc_times():





    #----------------------------------------------------------------------------------------------------------
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    begin_d = request.args.get('begin_date', 999, type = str)
    begin_t = request.args.get('begin_time', 999, type = str)
    print("begin_d={}".format(begin_d))
    print("request.args: {}".format(request.args))
    time_and_date = "{}T{}".format(begin_d, begin_t)
    time = arrow.get(time_and_date)






#----------------------------------------------------------------------------------------------------------
    brevet = request.args.get('distance', 999, type = int)
    print("brevet={}".format(brevet))
    print("request.args: {}".format(request.args))
    open_time = acp_time.open_time(km, brevet, time.isoformat())
    close_time = acp_time.close_time(km, brevet, time.isoformat())
    #SEND data to acp_time
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)




        #----------------------------------------------------------------------------------------------------------





if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
